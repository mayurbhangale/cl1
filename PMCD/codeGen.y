%{
#include<stdio.h>
#include<ctype.h>
#include<string.h>
FILE *fout;
%}

%token<var> NAME PLUS EQUAL MULT DIVI SUBT
%type<var> exp
%union
{
char var[10];
}
%right EQUAL
%left PLUS SUBT
%left MULT DIVI
%%

input:line '\n' input
|'\n'input
|/*empty*/
;
line:NAME EQUAL exp {fprintf(fout,"MOV %s,RX\n",$1);}
;
exp:NAME PLUS NAME {fprintf(fout,"MOV RX,%s \n ADD RX,%s\n",$1,$3);}
|NAME SUBT NAME {fprintf(fout,"MOV RX,%s \n SUB RX,%s\n",$1,$3);}
|NAME MULT NAME {fprintf(fout,"MOV RX,%s \n MUL RX,%s\n",$1,$3);}
|NAME DIVI NAME {fprintf(fout,"MOV RX,%s \n DIV RX,%s\n",$1,$3);}
|NAME {strcpy($$,$1);}
;
%%

extern yylineno;

yyerror()
{
printf("\nerror %d",yylineno);
}
yywrap()
{
return 1;
}
extern FILE *yyin;
main()

{

FILE *fin;
fin=fopen("input1.txt","r");
fout=fopen("out.txt","w");

yyin=fin;

yyparse();
fcloseall();
return 0;
}
