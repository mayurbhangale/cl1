%{
#include<stdio.h>
#include<string.h>
#include "y.tab.h"
%}

%%
[a-z][a-z0-9]* {strcpy(yylval.var,yytext);return NAME;}
[+] {strcpy(yylval.var,yytext);return PLUS;}
[=] {strcpy(yylval.var,yytext);return EQUAL;}
[-] {strcpy(yylval.var,yytext);return SUBT;}
[*] {strcpy(yylval.var,yytext);return MULT;}
[/] {strcpy(yylval.var,yytext);return DIVI;}
[\n\t] {return yytext[0];}
%%

------------------------YACC FILE-------------------------
%{
#include<stdio.h>
#include<ctype.h>
#include<string.h>
FILE *fout;
%}

%token<var> NAME PLUS EQUAL MULT DIVI SUBT
%type<var> exp
%union
{
char var[10];
}
%right EQUAL
%left PLUS SUBT
%left MULT DIVI
%%

input:line '\n' input
|'\n'input
|/*empty*/
;
line:NAME EQUAL exp {fprintf(fout,"MOV %s,RX\n",$1);}
;
exp:NAME PLUS NAME {fprintf(fout,"MOV RX,%s \n ADD RX,%s\n",$1,$3);}
|NAME SUBT NAME {fprintf(fout,"MOV RX,%s \n SUB RX,%s\n",$1,$3);}
|NAME MULT NAME {fprintf(fout,"MOV RX,%s \n MUL RX,%s\n",$1,$3);}
|NAME DIVI NAME {fprintf(fout,"MOV RX,%s \n DIV RX,%s\n",$1,$3);}
|NAME {strcpy($$,$1);}
;
%%

extern yylineno;

yyerror()
{
printf("\nerror %d",yylineno);
}
yywrap()
{
return 1;
}
extern FILE *yyin;
main()

{

FILE *fin;
fin=fopen("input1.txt","r");
fout=fopen("out.txt","w");

yyin=fin;

yyparse();
fcloseall();
return 0;
}
/*
----------------------OUTPUT---------------------------------
[admin@172-15-0-21 ~]$ lex target.l
[admin@172-15-0-21 ~]$ yacc -d target.y
[admin@172-15-0-21 ~]$ gcc lex.yy.c y.tab.c
[admin@172-15-0-21 ~]$ ./a.out
[admin@172-15-0-21 ~]$ cat input1.txt
*/
