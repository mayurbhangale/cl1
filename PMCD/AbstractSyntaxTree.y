%{
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct node
{
  struct node *left;
struct node *right;
char *token;
}node;
node *mknode(node *left, node *right, char *token);
void printtree(node *tree);

#define YYSTYPE node*
%}

%token NUM
%token PLUS MINUS MUL
%token LP RP
%token END

%left PLUS MINUS
%left MUL

%%
line1: exp END
{
printf("ABSTRACT TREE IS:");
printtree($1);
printf("\n");
}
;

exp:term {$$=$1;}
   |exp PLUS term {$$=mknode($1,$3,"+");}
   |exp MINUS term {$$=mknode($1,$3,"-");}
   ;

term:factor {$$=$1;}
    |term MUL factor {$$=mknode($1,$3,"*");}
    ;

factor:NUM {char buf[10];
       snprintf(buf,sizeof(buf),"%d",yylval);
       $$=mknode(0,0,buf);}
       |LP exp RP {$$=$2;}
       ;
%%

int main(void) {return yyparse();}

node *mknode(node *left, node *right, char *token)
{
  node *newnode=(node *)malloc(sizeof(node));
  char *newstr=(char *)malloc (strlen(token)+1);
  strcpy(newstr, token);
  newnode->left=left;
  newnode->right=right;
  newnode->token=newstr;
  return(newnode);
}

void printtree(node *tree)
{
if(tree->left|| tree->right)
printf("(");
printf("%s",tree->token);
if(tree->left)
printtree(tree->left);
if(tree->right)
printtree(tree->right);
if(tree->left|| tree->right)
printf(")");
}
int yyerror(char *s) {fprintf(stderr,"%s\n",s);}
