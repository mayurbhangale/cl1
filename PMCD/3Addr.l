/*Write YACC program to genrate 3-address code for subset of 'C' using quadruples and symbol table.
***********************************************************
%{
   #include "y.tab.h"
   extern char yyval;
%}

NUMBER [0-9]+
LETTER [a-zA-Z]+

%%
{NUMBER} {yylval.sym=(char)yytext[0]; return NUMBER;}
{LETTER} {yylval.sym=(char)yytext[0];return LETTER;}

\n {return 0;}
.  {return yytext[0];}

%%
*************yacc******************************

%{
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
void ThreeAddressCode();
void qudraple();
char AddToTable(char ,char, char);

int ind=0;
char temp='A';
struct incod
{
  char arg1;
  char arg2;
  char op;
}code[20];
%}

%union
{
 char sym;
}

%token <sym> LETTER NUMBER
%type <sym> expr
%left '-''+'
%right '*''/'

%%

statement: LETTER '=' expr ';' {AddToTable((char)$1,(char)$3,'=');}
           | expr ';'
	   ;

expr: expr '+' expr {$$ = AddToTable((char)$1,(char)$3,'+');}
      | expr '-' expr {$$ = AddToTable((char)$1,(char)$3,'-');}
      | expr '*' expr {$$ = AddToTable((char)$1,(char)$3,'*');}
      | expr '/' expr {$$ = AddToTable((char)$1,(char)$3,'/');}
      | '(' expr ')' {$$ = (char)$2;}
      | NUMBER {$$ = (char)$1;}
      | LETTER {$$ = (char)$1;}
      ;

%%

yyerror(char *s)
{
  printf("%s",s);
  exit(0);
}

int id=0;

char AddToTable(char arg1,char arg2,char op)
{
code[ind].arg1=arg1;
code[ind].arg2=arg2;
code[ind].op=op;
ind++;
temp++;
return temp;
}

void ThreeAddressCode()
{
int cnt=0;
temp++;
printf("\n\n\t THREE ADDRESS CODE\n\n");
while(cnt<ind)
{
	printf("%c  = \t",temp);


        if(isalpha(code[cnt].arg1))
		printf("%c\t",code[cnt].arg1);
	else
		{printf("%c\t",temp);}

	printf("%c\t",code[cnt].op);

	if(isalpha(code[cnt].arg2))
		printf("%c\t",code[cnt].arg2);
	else
		{printf("%c\t",temp);}

	printf("\n");
	cnt++;
	temp++;
}
}

void quadraple()
{
int cnt=0;
temp++;
printf("\n\n\t QUADRUPLE CODE\n");
printf("IND\tOP\tARG1\tARG2\tRES\n");
while(cnt<ind)
{
printf("%d",id);
printf("\t");
printf("%c",code[cnt].op);
printf("\t");
     if(isalpha(code[cnt].arg1))
	printf("%c\t",code[cnt].arg1);
        else
	{printf("%c\t",temp);}
    if(isalpha(code[cnt].arg2))
	printf("%c\t",code[cnt].arg2);
	else
	{printf("%c\t",temp);}
		printf("%c",temp);
	printf("\n");
	cnt++;
	temp++;
	id++;

}
}

extern FILE*yyin;
main()
{
char fnm[20];
printf("Enter the File name:");
scanf("%s",&fnm);
yyin=fopen(fnm,"r");
yyparse();
temp='A';
ThreeAddressCode();
quadraple();
}

yywrap()
{
 return 1;
}
**************************output****************
[admin@localhost ~]$ cat a.txt
a=((b+c)*(d/e));
[admin@localhost ~]$ lex prog5.l
[admin@localhost ~]$ yacc -d prog5.y
[admin@localhost ~]$ gcc lex.yy.c y.tab.c
[admin@localhost ~]$ ./a.out
Enter the File name:a.txt


	 THREE ADDRESS CODE

B  = 	b	+	c
C  = 	d	/	e
D  = 	B	*	C
E  = 	a	=	D


	 QUADRUPLE CODE
IND	OP	ARG1	ARG2	RES
0	+	b	c	G
1	/	d	e	H
2	*	B	C	I
3	=	a	D	J
[admin@localhost ~]$










Name: Rajesh Pawar
Roll No: 309
Batch: B2
Div: B
College Name:SKN-SITS,Lonavala

/*Write YACC program to genrate 3-address code for subset of 'C' using quadruples and symbol table.*/
*************lex*******************
%{
#include "y.tab.h"
int lineno=1,flag=1;
extern FILE *fp;
%}
%%
"\n"      {lineno++;}
[ \t]+    {}
[0-9]+    {yylval.no=atoi(yytext); return digit;}
"<=" |
">=" |
"<" |
">" |
"!=" |
"=="  {if(flag==1) {printf("%s", yytext);} return rel_op;}
"{" {return brace_op;}
"}" {return brace_cls;}
"(" {printf("("); return para_op;}
")" {printf(")"); flag=0; return para_cls;}
"if"      {printf("if"); flag=1; return if_id;}
"else" {printf("else"); flag=1; return else_id;}
[a-z]+    { strcpy(yylval.str,yytext); if(flag==1) {printf("%s", yytext);} return id;}
"$"       {return(yytext[0]);}
.         {  return(yytext[0]);}
%%
int yywrap()
{
return 1;
}

yyerror(char *s)
{
printf("%s at line no %d for %s ",s, lineno,yytext);
exit(1);
}







********************yacc*********************************
%{
 #include<stdio.h>
 #include<stdlib.h>
 #include<string.h>
 typedef struct intercode
 {
  char optor;
  char op1[10];
  char op2[10];
  char res[10];
 }intercode;
 intercode ic_if[50],ic_else[50];
 int allrows,temp=1,cnt_if=0;
char goto_if[]="goto L0\0",goto_else[]="goto L1\0";
%}

%union
{
int no;
char str[10];
}

%token <str> if_id
%token <str> else_id
%token <str> rel_op
%token <str> brace_cls
%token <str> brace_op
%token <str> para_op
%token <str> para_cls
%token <str> id
%token <no> digit
%type <str> expr

%left '+' '-'
%left '*' '/'
%nonassoc '='

%%
start : if_id para_op id rel_op id para_cls brace_op start {}
| '$'   {exit(0); }
| id '=' expr ';'{ emit_if('=',$3,"",$1); yyparse();}
|brace_cls{printf("goto L0 \n goto L1");
int i=0;
                printf("\n \t L0 :\t" );
                for(i=0;i<cnt_if;i++)
{
printf("%c",ic_if[i].optor);
printf("\t\t %s",ic_if[i].op1);
printf("\t\t %s",ic_if[i].op2);
printf("\t\t %s\n\t\t",ic_if[i].res);
}
yyparse();
}
      ;

expr : expr'+'expr {sprintf($$,"t%d",temp); temp++; emit_if('+',$1,$3,$$);}
     | expr'-'expr {sprintf($$,"t%d",temp); temp++; emit_if('-',$1,$3,$$);}
     | expr'*'expr {sprintf($$,"t%d",temp); temp++; emit_if('*',$1,$3,$$);}
     | expr'/'expr {sprintf($$,"t%d",temp); temp++; emit_if('/',$1,$3,$$);}
     |id {}
     |digit{}
     ;
%%
main()
{
printf("enter the expression");
yyparse();
return 0;
}
void emit_if(char opr,char *op1, char *op2,char *res)
{
 ic_if[cnt_if].optor=opr;
 strcpy(ic_if[cnt_if].op1,op1);
 strcpy(ic_if[cnt_if].op2,op2);
 strcpy(ic_if[cnt_if].res,res);
 cnt_if++;
}
/*
**********************output*****************************
[admin@localhost ~]$ lex prog4.l
[admin@localhost ~]$ yacc -d prog4.y
[admin@localhost ~]$ gcc lex.yy.c y.tab.c
[admin@localhost ~]$ ./a.out
enter the expressionif(a<b)
if(a<b)
{
 x=a+b;
 y=a-b;
}
goto L0
 goto L1
 	 L0 :	+		 a		 b		 t1
		=		 t1		 		 x
		-		 a		 b		 t2
		=		 t2		 		 y

*/
