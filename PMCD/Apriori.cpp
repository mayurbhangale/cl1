#include<iostream>
#include<cstring>
using namespace std;

int main(){
    int i,j,k,l,f1,f2,nt=5,ni=6,nui=12;       // nt=No. of transaction ,ni=No. of itm set, nui= No. of unique itemset
    //Initial item-purchase
    char a[nt][ni][15],itmset[nui][15];
    char l1itmset[nui][15];
    int support[nui];  //support value

    cout<<"\n No. of transaction ";
    cin>>nt;
    cout<<"\n No. of itemset ";
    cin>>ni;
    cin.clear();

    for(i=0;i<nt;i++){
        cout<<"\n Enter items from purchase "<<i+1<<":";

        for(j=0;j<ni;j++){
            cin>>a[i][j];
            cin.clear();
          }
          cin.clear();
    }

//Initialization of support
    for(i=0;i<nui;i++){
      support[i]=0;
    }

    cout<<"\n No. of unique itemset ";
    cin>>nui;
    cout<<"\n Enter Unique item set :\n";

    for(j=0;j<nui;j++){
        cout<<"\nItemset "<<j+1<<": ";
        cin>>itmset[j];
        cin.clear();
    }

    cout<<"\n Transaction Table :\n";
    cout<<"Trancaction   ";

    for(i=0;i<ni;i++){
      cout<<"\titem "<<i+1;
    }
    cout<<"\n------------------------------------------------------------\n";

    for(i=0;i<nt;i++){
      cout<<"Transaction"<<i+1<<"   ";
      for(j=0;j<ni;j++){
          cout<<a[i][j]<<"\t";
      }
      cout<<endl;
    }
    cout<<"\n------------------------------------------------------------\n";
    //Defining minimum level for acceptence
    int min;
    cout<<"\n Enter minimum acceptance level : ";
    cin>>min;

    for(i=0;i<nt;i++){
      for(j=0;j<ni;j++){
        for(k=0;k<nui;k++){
          if(strcmp(a[i][j],itmset[k])==0){
            support[k]++;
          }
        }
      }
    }
    //disp
    cout<<"\nSupport Count of Itemset:\n";
    cout<<"\nITEMS\t\tSUPPORT\n------------------------------------------------------------\n";

    for(j=0;j<nui;j++){
        cout<<itmset[j]<<"\t\t"<<support[j]<<"\n";
    }
    cout<<"\n------------------------------------------------------------\n";

    int count=0,cnt=0;
    cout<<"Generating level 1 Itemset:\n\n";
    cout<<"ITEMS\t\tSUPPORT\n------------------------------------------------------------\n";
    for(i=0;i<nui;i++){
        if(support[i]>=min){
            strcpy(l1itmset[count],itmset[i]);
            cout<<itmset[i]<<"\t\t"<<support[i]<<endl;
            count++;
          }
    }
    cout<<"\n------------------------------------------------------------";
    cout<<"\n Generating Level 2";
    cout<<"\n------------------------------------------------------------";
    cout<<"\nITEMS\t\tSUPPORT\n------------------------------------------------------------\n";

    for(i=0;i<count;i++){
       for(j=i+1;j<count;j++){
         cnt=0;	    // cout<<l1itmset[i]<<"   "<<l1itmset[j]<<"\n";
    	    for(k=0;k<nt;k++){
            f1=0;f2=0;
    		      for(l=0;l<ni;l++){
    			         if(strcmp(l1itmset[i],a[k][l])==0){
    				            f1=1;
    			          }
    			         if((strcmp(l1itmset[j],a[k][l]))==0){
                     		f2=1;
    			         }
    			         if(f1==1 && f2==1){
    				             cnt++;
    				             f1=0;f2=0;
    			         }
    		     }
    	    }
          if(cnt>=min){
    		      cout<<l1itmset[i]<<"\t"<<l1itmset[j]<<"\t"<<cnt<<"\n";
            }
       }
    }
    cout<<"------------------------------------------------------------\n";
    return 0;
    }
