%{
#include "y.tab.h"
extern int yylval;
%}

%%
[0-9]+ {yylval=atoi(yytext); return NUM;}
[ \t\n]+ ;
"+" return(PLUS);
"-" return(MINUS);
"*" return(MUL);
"(" return(LP);
")" return(RP);
";" return(END);
%%
int yywrap(void) {return 1;}
