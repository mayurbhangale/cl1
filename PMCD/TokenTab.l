%{

	#include<stdio.h>
	#include<string.h>
	struct st
	{
		char LEXeme[25];
		char name[25];
	}ST[100];

	int cnt=0;

%}

ID [a-zA-Z][a-zA-Z0-9]*
DIGIT [0-9]
Keywords auto|double|int|struct|break|else|long|switch|case|enum|register|typedef|char|extern|return|union|continue|for|signed|void|do|if|static|while|default|goto|sizeof|volatile|const|float|short|unsigned
%%

{Keywords} {strcpy(ST[cnt].LEXeme,yytext);strcpy(ST[cnt].name,"Keyword");}cnt++;

{DIGIT}+ {strcpy(ST[cnt].LEXeme,yytext);strcpy(ST[cnt].name,"const integer literal");}cnt++;

{DIGIT}+"."{DIGIT}+ {strcpy(ST[cnt].LEXeme,yytext);strcpy(ST[cnt].name,"const float literal");}cnt++;

"#include"|"#define" {strcpy(ST[cnt].LEXeme,yytext);strcpy(ST[cnt].name,"ppdirective");}cnt++;

{ID}".h" {strcpy(ST[cnt].LEXeme,yytext);strcpy(ST[cnt].name,"include file");}cnt++;

main|void|switch|case|continue|break|do|while|for|if|float|char {strcpy(ST[cnt].LEXeme,yytext);strcpy(ST[cnt].name,"keyword");}cnt++;

"\""{ID}"\"" {strcpy(ST[cnt].LEXeme,yytext);strcpy(ST[cnt].name,"string literal");}cnt++;

{ID} {strcpy(ST[cnt].LEXeme,yytext);strcpy(ST[cnt].name,"identifier");}cnt++;

"+"|"-"|"*"|"/"|"%" {strcpy(ST[cnt].LEXeme,yytext);strcpy(ST[cnt].name,"Arithmetic OP");}cnt++;

"&"|"|"|"^"|"+"|"~" {strcpy(ST[cnt].LEXeme,yytext);strcpy(ST[cnt].name,"Bitwise OP");}cnt++;

"<<"|">>" {strcpy(ST[cnt].LEXeme,yytext);strcpy(ST[cnt].name,"Bitwise Shift OP");}cnt++;

"&&"|"||"|"!" {strcpy(ST[cnt].LEXeme,yytext);strcpy(ST[cnt].name,"Logical OP");}cnt++;

"<"|">"|"<="|">=" {strcpy(ST[cnt].LEXeme,yytext);strcpy(ST[cnt].name,"Relational OP");}cnt++;

"=="|"!=" {strcpy(ST[cnt].LEXeme,yytext);strcpy(ST[cnt].name,"Equality OP");}cnt++;

"[" {strcpy(ST[cnt].LEXeme,yytext);strcpy(ST[cnt].name,"OSB");}cnt++;

"]" {strcpy(ST[cnt].LEXeme,yytext);strcpy(ST[cnt].name,"CSB");}cnt++;

"{" {strcpy(ST[cnt].LEXeme,yytext);strcpy(ST[cnt].name,"OCB");}cnt++;

"}" {strcpy(ST[cnt].LEXeme,yytext);strcpy(ST[cnt].name,"CCB");}cnt++;

"(" {strcpy(ST[cnt].LEXeme,yytext);strcpy(ST[cnt].name,"ORB");}cnt++;

")" {strcpy(ST[cnt].LEXeme,yytext);strcpy(ST[cnt].name,"CRB");}cnt++;

";" {strcpy(ST[cnt].LEXeme,yytext);strcpy(ST[cnt].name,"Semicolon");}cnt++;

"++" {strcpy(ST[cnt].LEXeme,yytext);strcpy(ST[cnt].name,"Inc OP");}cnt++;

"--" {strcpy(ST[cnt].LEXeme,yytext);strcpy(ST[cnt].name,"Dec OP");}cnt++;

"?" {strcpy(ST[cnt].LEXeme,yytext);strcpy(ST[cnt].name,"Ternary OP");}cnt++;

"=" {strcpy(ST[cnt].LEXeme,yytext);strcpy(ST[cnt].name,"Assignment OP");}cnt++;

%%

int main()
{
	int i=0;
	FILE *f1;
	char fnm[20];
	printf("Enter the File name:");
	scanf("%s",&fnm);
	f1=fopen(fnm,"r");
	if(f1==NULL)
	{
		printf("File not found.....");
	}
	yyin=f1;
	yylex();
	printf("\t\t    TOKEN TABLE\n");
	printf("\n\t   LEXeme\t\t\tNAME\n");
	printf("\t____________\t\t___________________\n");
	for(i=0;i<cnt;i++)
	{
		printf("\n\t%s",ST[i].LEXeme);
		printf("\t\t\t%s",ST[i].name);
	}
	printf("\n");
}
int yywrap()
{
  return 1;
}
